(uiop:define-package :triptolemus/job
  (:use :cl)
  (:import-from :bordeaux-threads
                #:condition-notify
                #:condition-wait
                #:join-thread
                #:make-condition-variable
                #:make-lock
                #:make-thread
                #:with-lock-held)
  (:import-from :local-time #:now #:timestamp-difference #:timestamp+)
  (:import-from :uiop #:run-program)
  (:export #:job-info
           #:job-thread
           #:last-run-error
           #:refresh-job
           #:start-job
           #:stop-job))

(in-package :triptolemus/job)

(defclass job-info ()
  ((name :initarg :name :reader name)
   (command :initarg :command :reader command)
   (interval :initarg :interval :reader interval)))

(defclass job-thread ()
  ((job-info :initarg :job-info :reader job-info)
   (lock :accessor lock)
   (condition-variable :accessor condition-variable)
   (thread :accessor thread)
   (stopped :accessor stopped :initform nil)
   (last-run-error :accessor last-run-error :initform nil)))

(defmethod initialize-instance :after ((job job-thread) &key)
  (setf (lock job) (make-lock))
  (setf (condition-variable job) (make-condition-variable))
  (setf (thread job) (make-thread (run-loop job)
                                  :name (format nil "Job thread ~a" (name (job-info job))))))

(defun start-job (job-info)
  (make-instance 'job-thread :job-info job-info))

(defun stop-job (job)
  (with-lock-held ((lock job))
    (setf (stopped job) t)
    (condition-notify (condition-variable job)))
  (join-thread (thread job)))

(defun refresh-job (job)
  "Verifies that the existing job has not changed its definition, and stop/start
  its thread if it did. In any case, always return the job-thread."
  job)

(defun run-loop (job)
  (lambda ()
    (block run-loop
      ;; One of the core design principles is that the runs of the various jobs
      ;; are spread out. The start time is what we use to ensure this happens.
      (with-lock-held ((lock job))
        ;; Let's make sure we can be stopped before the first run.
        (condition-wait
         (condition-variable job)
         (lock job)
         :timeout (timestamp-difference (start-time (interval (job-info job))) (now))))
      (loop
        (with-lock-held ((lock job))
          (when (stopped job)
            (return-from run-loop))
          (setf (last-run-error job)
                (handler-case
                    (progn (%run job) nil)
                  (error (e) e)))
          (condition-wait (condition-variable job)
                          (lock job)
                          ;; This is where an important design decision has been
                          ;; taken: is the interval counted from the beginning or
                          ;; the end of the job?  Triptolemus has taken the
                          ;; decision to do it from the end of the job.
                          :timeout (interval (job-info job))))))))

(defun start-time (interval)
  "Returns a time in the future based on the interval.

We define the start time as a random number in -half/+half of the defined
interval."
  (timestamp+ (now)
              (+ (random interval)
                 (/ interval 2))
              :sec))

(defun %run (job)
  (run-program (command (job-info job))
               :output :interactive
               :error-output :interactive))
