# triptolemus

triptolemus is a jobs manager. Its goal is two-fold:

- executing a set of jobs in an environment with potentially scarce resources.
- providing an http endpoint for easy monitoring by external services.

It achieves those goals by letting the jobs define an interval at which they
should run, but not a start time. This lets triptolemus define _when_ the jobs
are running, in order to avoid all of them running together.

The HTTP endpoint is exposed on port 4242 and returns 502 if one of the jobs
exited unsuccessfully.

## Installation

Download and install the .deb file. Then:

```
systemctl enable triptolemus@"$USER" # e.g. triptolemus@root if you want to run it as root
```

## Usage

### Jobs

triptolemus looks for jobs to run in `$XDG_CONFIG_HOME/triptolemus/jobs.d/`
(where `$XDG_CONFIG_HOME` is typically `~/.config`), unless the
`TRIPTOLEMUS_JOBS` environment variable is specified, in which case it will use
that folder.

The jobs files must have the `.tt` extension, and be in this format:

```
command = ls / | head
interval = 60
```

Comments can be added by prepending the line with a `#`. The command above is a
bit pointless, but shows that the command is run in `sh`.

If the interval is not specified, it defaults to 300.

You can send a SIGHUP signal to the triptolemus process to make it reload the
list of jobs. Typically though, you would use:

```
systemctl reload triptolemus@"$USER"
```

### HTTP endpoint

The HTTP endpoint is exposed on `http://localhost:4242/.well-known/health`. The
port can be changed by setting the `$PORT` environment variable.

The endpoint returns an empty response, with 2 possible statuses:

- 200 if all the jobs are running correctly
- 502 if any jobs' last run has failed

## License

MIT.
