(uiop:define-package :triptolemus/jobs
  (:use :cl :triptolemus/job)
  (:import-from :alexandria #:hash-table-keys)
  (:import-from :bordeaux-threads
                #:condition-notify
                #:condition-wait
                #:join-thread
                #:make-condition-variable
                #:make-lock
                #:make-thread
                #:with-lock-held)
  (:import-from :uiop #:split-string)
  (:export #:is-healthy
           #:jobs-manager
           #:refresh-jobs
           #:stop-manager))

(in-package :triptolemus/jobs)

(defclass jobs-manager ()
  ((jobs-directory :initarg :jobs-directory :reader jobs-directory)
   (lock :accessor lock)
   (condition-variable :accessor condition-variable)
   (thread :accessor thread)
   (job-threads :accessor job-threads :initform nil)
   (stopped :accessor stopped :initform nil)))

(defmethod initialize-instance :after ((manager jobs-manager) &key)
  (setf (lock manager) (make-lock))
  (setf (condition-variable manager) (make-condition-variable))
  (setf (job-threads manager) (make-hash-table :test #'equal))
  (setf (thread manager) (make-thread (refresh-loop manager) :name "Manager thread")))

(defun is-healthy (manager)
  (with-lock-held ((lock manager))
    (maphash (lambda (job-name thread)
               (let ((last-run-error (last-run-error thread)))
                 (format (if last-run-error *error-output* t)
                         "~a last run error: ~a~%"
                         job-name
                         last-run-error)
                 (when last-run-error
                   (return-from is-healthy nil))))
             (job-threads manager))
    t))

(defun refresh-jobs (manager)
  (with-lock-held ((lock manager))
    (condition-notify (condition-variable manager))))

(defun stop-manager (manager)
  (with-lock-held ((lock manager))
    (setf (stopped manager) t)
    (condition-notify (condition-variable manager)))
  (join-thread (thread manager))
  (setf (job-threads manager) (make-hash-table :test #'equal)))

(defun refresh-loop (manager)
  (lambda ()
    (block refresh-loop
      (loop
        (with-lock-held ((lock manager))
          (when (stopped manager)
            (maphash (lambda (job-name thread)
                       (format t "stopping thread ~a~%" job-name)
                       (stop-job thread))
                     (job-threads manager))
            (return-from refresh-loop))
          (%refresh-jobs manager)
          (condition-wait (condition-variable manager) (lock manager)))))))

(defun %refresh-jobs (manager)
  (let* ((new-job-threads (make-new-job-threads manager)))
    (maphash
     (lambda (job-name thread)
       (unless (gethash job-name (job-threads manager))
         (stop-job thread)))
     (job-threads manager))
    (setf (job-threads manager) new-job-threads)))

(defun make-new-job-threads (manager)
  (let ((new-job-threads (make-hash-table :test #'equal))
        (old-job-threads (job-threads manager)))
    (dolist (job-info-file
             (directory
              (make-pathname :directory (pathname-directory (jobs-directory manager))
                             :name :wild
                             :type "tt")))
      (let ((job-name (pathname-name job-info-file)))
        (handler-case
            (multiple-value-bind (thread presentp)
                (gethash job-name old-job-threads)
              (setf (gethash job-name new-job-threads)
                    (if presentp
                        (refresh-job thread)
                        (start-job (parse-tt-file job-info-file)))))
          (error (e)
            (format *error-output* "failed starting thread for ~a: ~a~%" job-name e)))))
    new-job-threads))

(defun parse-tt-file (pathname)
  (let ((command) (interval))
    (with-open-file (stream pathname)
      (loop for line = (read-line stream nil 'eof)
            until (eq line 'eof)
            do (let* ((parts (mapcar (lambda (part)
                                       (string-downcase (string-trim '(#\Space #\Tab) part)))
                                     (split-string line :separator '(#\=))))
                      (key (first parts)))
                 (cond ((string= key "command")
                        (setf command (format nil "~{~a~^=~}" (rest parts))))
                       ((string= key "interval")
                        (setf interval (parse-integer (second parts))))))))
    (unless command
      (error "invalid tt file"))
    (make-instance 'job-info
                   :name (pathname-name pathname)
                   :command command
                   :interval (or interval 300))))
