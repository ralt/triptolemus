(defsystem "triptolemus"
  :defsystem-depends-on ("wild-package-inferred-system")
  :class "winfer:wild-package-inferred-system"
  :depends-on ("triptolemus/*"))

(defsystem "triptolemus/deb"
  :author "Florian Margaine <florian@margaine.com>"
  :description "Jobs manager."
  :license "MIT"
  :homepage "https://gitlab.com/ralt/triptolemus"
  :defsystem-depends-on ("linux-packaging")
  :class "linux-packaging:deb"
  :depends-on ("triptolemus")
  :build-operation "linux-packaging:build-op"
  :package-name "triptolemus"
  :additional-files (("service" . #p"/lib/systemd/system/triptolemus@.service"))
  :build-pathname "triptolemus"
  :entry-point "triptolemus/main:main")
