(uiop:define-package :triptolemus/main
  (:use :cl :triptolemus/jobs)
  (:import-from :hunchentoot
                #:define-easy-handler
                #:easy-acceptor
                #:return-code*
                #:start
                #:stop)
  (:import-from :trivial-signal #:signal-handler-bind)
  (:import-from :uiop
                #:getenv
                #:getenvp
                #:merge-pathnames*
                #:parse-unix-namestring
                #:quit
                #:unix-namestring
                #:xdg-config-home)
  (:export #:main))

(in-package :triptolemus/main)

(setf *random-state* (make-random-state t))

(defvar *acceptor* nil
  "Hunchentoot acceptor.")

(defvar *jobs-manager* nil
  "The custom job manager.")

(defun normalize-folder (path)
  (check-type path string)
  (concatenate
   'string
   (string-right-trim
    '(#\. #\/)
    (unix-namestring (merge-pathnames* (parse-unix-namestring path))))
   "/"))

(defun main ()
  (start-hunchentoot (parse-integer (or (getenv "PORT") "4242")))
  (start-jobs-manager (or (let ((jobs-env (getenv "TRIPTOLEMUS_JOBS")))
                            (when jobs-env
                              (normalize-folder jobs-env)))
                          (xdg-config-home #p"triptolemus/jobs.d/")))
  (signal-handler-bind ((:hup (lambda (signo)
                                (declare (ignore signo))
                                (refresh-jobs *jobs-manager*)))
                        (:term (lambda (signo)
                                 (declare (ignore signo))
                                 (stop-manager *jobs-manager*)
                                 (quit))))
    (sleep most-positive-fixnum)))

(defun start-hunchentoot (port)
  (when *acceptor*
    (stop *acceptor*))
  (setf *acceptor* (start (make-instance 'easy-acceptor :port port))))

(defun start-jobs-manager (jobs-directory)
  (setf *jobs-manager* (make-instance 'jobs-manager :jobs-directory jobs-directory)))

(define-easy-handler (health :uri "/.well-known/health") ()
  (setf (return-code*)
        (if (is-healthy *jobs-manager*) 200 502))
  "")
